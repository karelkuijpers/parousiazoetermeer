function LTrim(str)
{  var whitespace = new String(" \t\n\r");
   var s = new String(str);
   if (whitespace.indexOf(s.charAt(0)) != -1) {
      var j=0, i = s.length;
      while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
         j++;
      s = s.substring(j, i);
   }
   return s;
}
function RTrim(str)
{  var whitespace = new String(" \t\n\r");
   var s = new String(str);
   if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
      var i = s.length - 1;       // Get length of string
      while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
         i--;
      s = s.substring(0, i+1);
   }
   return s;
}
function Trim(str)
{  return RTrim(LTrim(str));}
function ConvDateFromLocal(fieldname)
{	ds=document.form1[fieldname].value;
	dy=Number(ds.substr(6,4));dm=Number(ds.substr(3,2))-1;dd=Number(ds.substr(0,2));
    CurDate = new Date(dy,dm,dd); 
    return CurDate.toString();
}
function IsDatum(str)
{	if (str.length>0 && str!="00-00-0000")
	{	
		if (/^(0?[1-9]|[1-2]\d|3[0-1])\-(0?[1-9]|1[0-2])\-(19|20)\d{2}$/.test(str))
		{
			var sd = str.split("-");
			var year=sd[2];
			if (sd[1]==2 && sd[0]>(((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 ))return false;
		}
		else return false;
	}
	return true;
}
function daysInFebruary (year){

      // February has 29 days in any year evenly divisible by four,

    // EXCEPT for centurial years which are not also divisible by 400.

    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );

}


function FormatDate(datum)
{
	// maak datum formaat dd-mm-jjjj
	var str=(Trim(datum.value)).toLowerCase();
	if (datum.value.length==0)return true;
	var re= /[/\s\.-]+/g
	str=str.replace(re,"-");
	re=/-+/g
	str=str.replace(re,"-");
	re=/([a-z]+)(\d+)/g
	str=str.replace(re,"$1-$2"); // splits tekst en getallen
	re=/(\d+)([a-z]+)/g
	str=str.replace(re,"$1-$2"); // splits tekst en getallen
	var sd = str.split("-");
	if (sd.length==1)
	{	if (!isNaN(sd[0]) && str.length>4)	str=str.substr(0,2)+"-"+str.substr(2,2)+"-"+str.substr(4);else str="01-01-"+sd[0];
		sd=str.split("-");
	}
	if (sd.length==2){str="01-"+sd[0]+"-"+sd[1];sd=str.split("-");}
	// vertaal eventuele maandnaam:
	if (isNaN(sd[1]))
	{/*	var mm={"jan":"1","feb":"2","maa":"3","apr":"4","mei":"5","jun":"6","jul":"7","aug":"8","sep":"9","okt":"10","oct":"10","nov":"11","dec":"12"};
		var mn=mm[sd[1].substr(0,3)]; */
		var mm={"ja":"01","f":"02","ma":"03","ap":"04","me":"05","jun":"06","jul":"07","au":"08","s":"09","o":"10","n":"11","d":"12"};
		var mn=mm[sd[1].substr(0,3)];
		if (!mn) mn=mm[sd[1].substr(0,2)];
		if (!mn) mn=mm[sd[1].substr(0,1)];
		if (mn)	sd[1]=mn; else sd[1]="01";
	}
	if (isNaN(sd[0])) sd[0]="01";
	if (isNaN(sd[1])) sd[1]="01";
	if (!isNaN(sd[2])&&sd[2].length<3)
	{	if (sd[2].length<2) sd[2]="0"+sd[2];
		if (sd[2].length<2) sd[2]="0"+sd[2];
		d=new Date();
		yr=(d.getFullYear()%100)+1;
		if (sd[2]>yr)sd[2]="19"+sd[2];else sd[2]="20"+sd[2];
	}
	if (sd[1].length<2)	{sd[1]='0'+sd[1];}
	if (sd[0].length<2)	{sd[0]='0'+sd[0];}
	str=sd[0]+"-"+sd[1]+"-"+sd[2];
	datum.value=str;
	if (!IsDatum(str))
	{	alert("Ongeldige datum!");
		return false;
	}
	return true;
}
function validateEmail(str)
{
	var re;
        // Rules for the email regular expression:
        // The start of the email must have at least one character 
        // before the @ sign
        // There may be either a . or a -, but not together before the @ sign
        // There must be an @ sign
        // At least once character must follow the @ sign
        // There may be either a . or a -, but not together in the address
        // The address must end with a . followed by at least 2 characters
	re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/;
	if (str.length>0) {return re.test(str);}
	return true;
}

// handle change of postcode and house number
function getstreetcity(e)
{
	var pc;
	var hnr;
	var reg = /^[1-9][0-9]{3}[A-Z]{2}$/;
	myrole=e.getAttribute("role");

	if (myrole=="postcode")
	{
	   pc=e.value;
	   pc=pc.replace(/ /g,'').substr(0,6).toUpperCase();
   	   if (!reg.test(pc))return false;  // no valid dutch postcode
	   e.value=pc.substr(0,4)+' '+pc.substr(4,2);
	   hnr=$(":input[role='huisnummer']").val();
	}
	else
	{
		hnr=e.value;
		pc=$(":input[role='postcode']").val();
	}
	atext=hnr.split(/\b|[a-zA-Z]/g);
	nr=atext[0];
	//alert("zoekpostcode pc:"+pc+"hnr:"+nr);
	if (pc.length==0 || nr.length==0) return;
	if(document.getElementById('postcodeorg') !== null)
	{
		var pcorg=$("#postcodeorg").val().replace(/ /g,'').substr(0,6).toUpperCase();
		var hnrorg=$("#huisnummerorg").val();
		var cityorg=$("#woonplaatsorg").val();
		city=$(":input[role='woonplaats']").val();
		if (pc==pcorg && hnr==hnrorg && city==cityorg)
	   	{
	    	$("#huisgenoten").hide();
			$("#allen_verhuizen").prop('checked',true);
		}
	}
	var formdata = new FormData();
	formdata.append('postcode',pc);
	formdata.append('nr',nr);
	$.ajax({
		//url: "/typo3conf/ext/churchpersreg/Classes/Hooks/zoekpostcode.php",
		url: "index.php?eID=zoekpostcode",
		data: formdata,
		dataType: "json",
		processData: false,
		contentType: false,
		type: 'POST',
		success: function (result) {
			//alert("postcode gevonden");
			//alert("status: "+result.status);
			if (result.status=="success")
			{
				woonplaats=result.woonplaats;
				if (woonplaats.length>0)
				{
					$(":input[role='straatnaam']").val(result.straat);
					$(":input[role='woonplaats']").val(woonplaats);
					//alert("straat:"+$('#persoon-straatnaam').val()+"; woonplaats:"+$('#persoon-woonplaats').val())
					if (pcorg)
	  				    if (pc!=pcorg || hnr!=hnrorg || woonplaats!=cityorg )$("#huisgenoten").show();
				}
			}
			else alert("Fout: " + result.message);
		},
		error: function (data, textStatus, jqXHR) {
			//alert("Error: " + jqXHR.status + ": " + jqXHR.statusText);
			$(":input[role='straatnaam']").val('');
			$(":input[role='woonplaats']").val('');

		}, 
	}); 
}

