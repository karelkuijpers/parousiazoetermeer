function multiSelect( elemId )
{  // select multiple without CTRL key; call with javascript: multiSelect( "id of select" ) to activate;
	var table = [], box = document.getElementById(elemId), lastSelected = -1, tempIdx, ctrl = false, shift = false;  
	// initialze table with preselected items:
	for(var i = 0;i < box.options.length;i++){ if (box.options[i].selected)table.push(i); } 
	   
	box.onkeyup = box.onkeydown = readCtrlKey; 
	box.onclick = function( /*28432953637269707465726C61746976652E636F6D*/ )
	{
		//alert("ctrl:"+ctrl+" shift:"+shift);
		//if (ctrl)alert("ctrl selected index:"+this.selectedIndex);
		if( !ctrl && !shift)
		{  
		   if( !inTable( this.selectedIndex ) )
		   {
			   table.push( this.selectedIndex );  
			  // alert ("push:"+this.selectedIndex);
			}
		    else
			{
				removeFromTable( this.selectedIndex ); 
			   //alert ("remove:"+this.selectedIndex);
				this.options[ this.selectedIndex ].selected = false;
		   }
		 }
		 else
		 {	
		 	//alert("table before:"+table.toString());
			for(var i = 0;i < box.options.length;i++)
			{ 
				if (box.options[i].selected && !inTable(i))table.push(i); 
			} 			
		 	//alert("table after:"+table.toString());
			ctrl = false, shift = false;
		 }
		 for( var i=0, len = table.length;  i<len ; i++ ) this.options[ table[i] ].selected = true;
	}
 
	function removeFromTable( idx )
	{
		 for( var i=0, len=table.length; i<len && table[i]!=idx; i++ ){;}
		 if (i<len) table.splice(i, 1);
	}
	 
	function inTable( idx )
	{
	  for( var i=0, len=table.length; i<len && table[i]!=idx; i++ ){;}
	  return i != len;  
	}
	 
	function readCtrlKey( evt )
	{
	  var e = evt || window.event;  
	  if( typeof( e.ctrlKey ) != 'undefined')ctrl = e.ctrlKey;   
	  if( typeof( e.shiftKey ) != 'undefined')shift = e.shiftKey;   
	  //alert ("readCtrlKey ctrl:"+ctrl+" shift:"+shift);
	} 
}

//--------------------------------------------------------------------------
// scroll to first selected of multiple selection 
//--------------------------------------------------------------------------

function scroll_selected ( form ) 
{
  var form = ( typeof form == 'string' ) ?  document.getElementById(form) : form;
  var selects = form.getElementsByTagName('select');
  OUTER: for ( var i = 0; i < selects.length; i ++ ) 
  {
  	var opts = selects.item(i).getElementsByTagName('option');
  	for ( var j = opts.length -1; j > 0; --j ) 
	{
  		if ( opts.item(j).selected == true ) 
		{
		  selects.item(i).scrollTop = j * opts.item(j).offsetHeight;
		  opts.item(j).selected = true;
		  continue OUTER;
		 }
 	}
  }
}

