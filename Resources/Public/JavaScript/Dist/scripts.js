/*!
 * Parousiazoetermeer v1.0.0 (https://parousiazoetermeer.nl)
 * Copyright 2017-2020 Karel Kuijpers
 * Licensed under the GPL-2.0-or-later license
 */
 $( function() {
    $( "#draggable" ).draggable();
  } );