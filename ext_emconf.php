<?php

/**
 * Extension Manager/Repository config file for ext "parousiazoetermeer".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Parousiazoetermeer',
    'description' => 'sitepackage',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-11.5.99',
            'fluid_styled_content' => '9.5.0-11.5.99',
            'rte_ckeditor' => '9.5.0-11.5.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'EvangelischeGemeenteParousiaZoetermeer\\Parousiazoetermeer\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Karel Kuijpers',
    'author_email' => 'karelkuijpers@gmail.com',
    'author_company' => 'Evangelische Gemeente Parousia Zoetermeer',
    'version' => '11.0.3',
];
