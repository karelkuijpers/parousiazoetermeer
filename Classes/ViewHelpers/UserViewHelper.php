<?php
namespace EvangelischeGemeenteParousiaZoetermeer\Parousiazoetermeer\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;


class UserViewHelper extends AbstractViewHelper
{
  //  use CompileWithRenderStatic;
	public function initializeArguments()
	{
		$this->registerArgument('fieldname', 'string', 'The name of the userfield to return', true);
	}

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ): string {
		if ($arguments['fieldname']=='nextsunday')
		{
			$date= new \DateTime();
	//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'userViewHelper daynbr: '.$date->format('N')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parousiazoetermeer/Classes/ViewHelpers/debug.log');
			if ($date->format('N')!=7)$date->modify('next sunday');
			return $date->format('d-m');
		}
		elseif ($arguments['fieldname']=='mysearch')
		{
			$mylogin= GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('parousiazoetermeer','pagemysearch');
			if (substr($mylogin,0,1)!='/') $mylogin='/index.php?id='.$mylogin;
			return $mylogin;
		}
		elseif ($arguments['fieldname']=='mylogin')
		{
			$mylogin= GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('parousiazoetermeer','pagemylogin');
			if (substr($mylogin,0,1)!='/') $mylogin='/index.php?id='.$mylogin;
			return $mylogin;
		}
		elseif ($arguments['fieldname']=='myprofile')
		{
			$myprofile= GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('parousiazoetermeer','pagemyprofile');
			if (substr($myprofile,0,1)!='/') $myprofile='/index.php?id='.$myprofile;
			return $myprofile;
		}
		elseif ($arguments['fieldname']=='mygifts')
		{
			$mygifts= GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('parousiazoetermeer','pagemygifts');
			if (substr($mygifts,0,1)!='/') $mygifts='/index.php?id='.$mygifts;
			return $mygifts;
		}
		elseif ($arguments['fieldname']=='mylogoff')
		{
			$mylogoff= GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('parousiazoetermeer','pagemylogoff');
			if (substr($mylogoff,0,1)!='/') $mylogoff='/index.php?id='.$mylogoff;
			return $mylogoff;
		}
		elseif ($arguments['fieldname']=='my2FA')
		{
			$my2FA= GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('parousiazoetermeer','pagemy2FA');
			if (substr($my2FA,0,1)!='/') $my2FA='/index.php?id='.$my2FA;
			return $my2FA;
		}
		elseif ($arguments['fieldname']=='privacystatement')
		{
			$mystmnt= GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('parousiazoetermeer','pageprivacystatement');
			if (substr($mystmnt,0,1)!='/') $mystmnt='/index.php?id='.$mystmnt;
			return $mystmnt;
		}
    }
}