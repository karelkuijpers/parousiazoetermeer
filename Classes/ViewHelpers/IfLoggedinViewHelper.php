<?php
namespace EvangelischeGemeenteParousiaZoetermeer\Parousiazoetermeer\ViewHelpers;

//use TYPO3\CMS\Core\Context\Context;
//use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

class IfLoggedinViewHelper extends AbstractConditionViewHelper
{
    /**
     * This method decides if the condition is TRUE or FALSE. It can be overridden in extending viewhelpers to adjust functionality.
     *
     * @param array $arguments ViewHelper arguments to evaluate the condition for this ViewHelper, allows for flexibility in overriding this method.
     * @return bool
     */
    protected static function evaluateCondition($arguments = null)
    {
		//$cookieset=(isset($_COOKIE['roepnaam']) ? stripslashes($_COOKIE['roepnaam']) : '');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'IfLoggedinViewHelper cookies : '.urldecode(http_build_query($_COOKIE,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parousiazoetermeer/Classes/ViewHelpers/debug.txt');
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'IfLoggedinViewHelper cookieset: '.isset($_COOKIE['roepnaam']).'; cookie: '.$_COOKIE['roepnaam'].'; stripped: '.stripslashes($_COOKIE['roepnaam'])."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parousiazoetermeer/Classes/ViewHelpers/debug.txt');
		//return !empty($cookieset);
		$loggedin=$GLOBALS['TSFE']->fe_user->isCookieSet();
		if (isset($GLOBALS['TSFE']->fe_user->user))
			error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'IfLoggedinViewHelper user : '.urldecode(http_build_query($GLOBALS['TSFE']->fe_user->user,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parousiazoetermeer/Classes/ViewHelpers/debug.txt');
		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'IfLoggedinViewHelper uitgelogd ses : '.$GLOBALS['TSFE']->fe_user->getKey('ses','uitgelogd').';  user : '.$GLOBALS['TSFE']->fe_user->getKey('user','uitgelogd')."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/parousiazoetermeer/Classes/ViewHelpers/debug.txt');
		return $loggedin;

		//return !empty($_SESSION["feuserid"]);
//        return GeneralUtility::makeInstance(Context::class)->getPropertyFromAspect('frontend.user', 'id', 0) > 0;
    }
}

