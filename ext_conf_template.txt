#
# Extension Configuration template
#
# Records
###########################
# cat=account; type=string; label=page_id Inloggen .e.g. 77 or /inloggen
pagemylogin= 77
# cat=account; type=string; label=page_id Personen zoeken.e.g. 96 or /contact/personen-zoeken
pagemysearch= /contact/personen-zoeken
# cat=account; type=string; label=page_id Mijn gegevens .e.g. 141 or /mijnprofiel
pagemyprofile= 141
# cat=account; type=string; label=page_id Mijn gavenprofiel .e.g. 297 or /gavenprofiel
pagemygifts= 297
# cat=account; type=string; label=page_id Uitloggen .e.g. 74 or /uitloggen
pagemylogoff= 76
# cat=account; type=string; label=page_id 2-staps verificatie .e.g. 76 or /twofactor
pagemy2FA= 76
# cat=footer; type=string; label=page_id Privacy statement .e.g. 288 or /privacyverklaring
pageprivacystatement= /privacyverklaring
